-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2024 at 01:07 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `htmlaundry-restapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenislayanan`
--

CREATE TABLE `jenislayanan` (
  `id_layanan` int(5) UNSIGNED NOT NULL,
  `nama_layanan` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `waktu_pengerjaan` varchar(50) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2023-12-08-073807', 'App\\Database\\Migrations\\Pegawai', 'default', 'App', 1702459392, 1),
(2, '2023-12-08-073811', 'App\\Database\\Migrations\\Pelanggan', 'default', 'App', 1702459392, 1),
(3, '2023-12-08-073902', 'App\\Database\\Migrations\\JenisLayanan', 'default', 'App', 1702459921, 2),
(4, '2023-12-13-093246', 'App\\Database\\Migrations\\AntarJemput', 'default', 'App', 1702460027, 3),
(5, '2023-12-13-093357', 'App\\Database\\Migrations\\Transaksi', 'default', 'App', 1702460081, 4),
(7, '2023-12-14-091828', 'App\\Database\\Migrations\\TransaksiLayanan', 'default', 'App', 1702546286, 5),
(19, '2023-12-14-093157', 'App\\Database\\Migrations\\Transaksi', 'default', 'App', 1702724061, 6),
(20, '2023-12-14-144529', 'App\\Database\\Migrations\\CreateTransaksiLayanan', 'default', 'App', 1702724061, 6);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(5) UNSIGNED NOT NULL,
  `nama` varchar(55) NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `alamat` varchar(14) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama`, `no_hp`, `alamat`, `username`, `password`, `created_at`, `updated_at`) VALUES
(3, 'rapi', 'iesnsnasn', 'panam', 'mitsjnsa', '$2y$10$Klx1F.x1V6G8A5njbhaOX.0LGJzIrjknZxL2twQdbAr', '2024-01-15 10:36:54', '2024-01-16 06:53:01'),
(6, 'rapi', 'iesnsnasn', 'panam', 'jajaja', '$2y$10$FeZFl6H64N5rYcZ2UjW0cecnvLu5frTxeV5E9rMLy.A', '2024-01-15 10:42:19', '2024-01-16 06:43:37'),
(8, 'mita', 'iesnsnasn', 'panam', '54454545', '$2y$10$1zu7NGP33jylhUXoqyWaOeb/N3ewJVV16VOoAqYDQf6', '2024-01-16 06:39:01', '2024-01-16 06:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(5) UNSIGNED NOT NULL,
  `nama` varchar(55) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `password` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama`, `no_hp`, `alamat`, `password`, `username`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'mita', '086421325545', 'rumbai', 'mita123', 'mita123', '', '2023-12-13 10:10:45', '2023-12-13 10:10:45'),
(3, 'yuli', '084466331122', 'umban sari atas', '$2y$10$lDc', 'yuli', '', '2023-12-14 14:31:56', '2023-12-14 14:31:56');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(5) UNSIGNED NOT NULL,
  `id_pegawai` int(5) UNSIGNED NOT NULL,
  `id_pelanggan` int(5) UNSIGNED NOT NULL,
  `total_harga` decimal(14,2) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `keluhan` varchar(300) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksilayanan`
--

CREATE TABLE `transaksilayanan` (
  `id` int(5) UNSIGNED NOT NULL,
  `id_transaksi` int(5) UNSIGNED NOT NULL,
  `id_layanan` int(5) UNSIGNED NOT NULL,
  `berat` int(4) NOT NULL,
  `total_harga` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenislayanan`
--
ALTER TABLE `jenislayanan`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `transaksi_id_pegawai_foreign` (`id_pegawai`),
  ADD KEY `transaksi_id_pelanggan_foreign` (`id_pelanggan`);

--
-- Indexes for table `transaksilayanan`
--
ALTER TABLE `transaksilayanan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaksi_id_layanan` (`id_transaksi`,`id_layanan`),
  ADD KEY `transaksilayanan_id_layanan_foreign` (`id_layanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenislayanan`
--
ALTER TABLE `jenislayanan`
  MODIFY `id_layanan` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksilayanan`
--
ALTER TABLE `transaksilayanan`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_id_pegawai_foreign` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`),
  ADD CONSTRAINT `transaksi_id_pelanggan_foreign` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`);

--
-- Constraints for table `transaksilayanan`
--
ALTER TABLE `transaksilayanan`
  ADD CONSTRAINT `transaksilayanan_id_layanan_foreign` FOREIGN KEY (`id_layanan`) REFERENCES `jenislayanan` (`id_layanan`),
  ADD CONSTRAINT `transaksilayanan_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
