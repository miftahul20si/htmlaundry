<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

// Rute untuk Aplikasi Web
$routes->group('web', ['namespace' => 'App\Controllers\Web'], function ($routes) {
    $routes->get('pegawai', 'pegawaiController::index');
    $routes->get('pegawai/create', 'PegawaiController::create');
    $routes->post('pegawai/store', 'PegawaiController::store');
    $routes->get('pegawai/edit/(:num)', 'PegawaiController::edit/$1');
    $routes->post('pegawai/update/(:num)', 'PegawaiController::update/$1');
    $routes->delete('pegawai/delete/(:num)', 'PegawaiController::delete/$1');

    $routes->get('pelanggan', 'PelangganController::index');
    $routes->post('pelanggan/create/', 'PelangganController::create');
    $routes->post('pelanggan/update/(:num)', 'PelangganController::update/$1');
    $routes->delete('pelanggan/delete/(:num)', 'PelangganController::delete/$1');

    $routes->get('jenislayanan', 'JenisLayananController::index');
    $routes->post('jenislayanan/create/', 'JenisLayananController::create');
    $routes->post('jenislayanan/update/(:num)', 'jenislayananController::update/$1');
    $routes->delete('jenislayanan/delete/(:num)', 'jenislayananController::delete/$1');

    $routes->post('transaksi/create/', 'TransaksiController::create');
});


// Rute untuk API
$routes->group('api', ['namespace' => 'App\Controllers\Api'], function ($routes) {
    $routes->get('pegawai', 'pegawaiController::index');
    $routes->post('pegawai/create/', 'PegawaiController::create');
    $routes->post('pegawai/update/(:num)', 'PegawaiController::update/$1');
    $routes->delete('pegawai/delete/(:num)', 'PegawaiController::delete/$1');

    $routes->get('pelanggan', 'PelangganController::index');
    $routes->post('pelanggan/create/', 'PelangganController::create');
    $routes->post('pelanggan/update/(:num)', 'PelangganController::update/$1');
    $routes->delete('pelanggan/delete/(:num)', 'PelangganController::delete/$1');

    $routes->get('jenislayanan', 'JenisLayananController::index');
    $routes->post('jenislayanan/create/', 'JenisLayananController::create');
    $routes->post('jenislayanan/update/(:num)', 'jenislayananController::update/$1');
    $routes->delete('jenislayanan/delete/(:num)', 'jenislayananController::delete/$1');
});


// $routes->get('/', 'Home::index');

// $routes->resource('ApiPegawaiController');

// $routes->post('pegawai/ubah/(:num)', 'PegawaiController::update/$1');
// $routes->get('pegawai/showList', 'PegawaiController::showList');
// $routes->get('pegawai/showForm', 'PegawaiController::showForm');

// $routes->resource('pelanggan', ['controller' => 'PelangganController']);
// $routes->post('pelanggan/ubah/(:num)', 'PelangganController::update/$1');
// $routes->get('pelanggan', 'pelangganController::index');


// $routes->resource('jenislayanan', ['controller' => 'JenisLayananController']);
// $routes->post('jenislayanan/ubah/(:num)', 'JenisLayananController::update/$1');

// // Tambahkan rute untuk menampilkan form transaksi
// $routes->get('transaksi/form', 'TransaksiController::showForm');
// $routes->resource('transaksi', ['controller' => 'TransaksiController']);
// $routes->post('transaksi/ubah/(:num)', 'TransaksiController::update/$1');

// $routes->group('pegawai', ['namespace' => 'App\Controllers'], function($routes){
    // Rute sumber daya pegawai
    // $routes->resource('pegawai', ['controller' => 'PegawaiController']);

    // Rute khusus untuk membuat data baru (formulir)
    // $routes->get('/', 'PegawaiController::tampil');
    // $routes->post('update/(:num)', 'PegawaiController::update/$1');
    // $routes->add('tambah', 'PegawaiController::create');

    // Rute kustom untuk melakukan aksi tertentu
    // $routes->add('custom-action', 'PegawaiController::customAction');
// });
