<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Transaksi Layanan</title>
</head>

<body>
    <h2>Create Transaksi Layanan</h2>

    <form action="<?= base_url('transaksilayanan/create'); ?>" method="post">
        <label for="id_transaksi">transaksi ID:</label>
        <select name="id_transaksi" required>
            <?php foreach ($transaksi as $transaksi) : ?>
                <option value="<?= $transaksi['id']; ?>"><?= $transaksi['transaksi_name']; ?></option>
            <?php endforeach; ?>
        </select><br>

        <label for="id_layanan">Layanan ID:</label>
        <select name="id_layanan" required>
            <?php foreach ($layanan as $layanan) : ?>
                <option value="<?= $layanan['id']; ?>"><?= $layanan['layanan_name']; ?></option>
            <?php endforeach; ?>
        </select><br>

        <input type="submit" value="Submit">
    </form>
</body>

</html>