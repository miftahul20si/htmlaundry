<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Transaksi Layanan</title>
</head>

<body>
    <h2>Create Transaksi Layanan</h2>

    <form action="/web/transaksi/create" method="post">
        <label for="id_pelanggan">ID Pelanggan:</label>
        <input type="text" name="id_pelanggan" required><br>

        <label for="tanggal_masuk">Tanggal Masuk:</label>
        <input type="date" name="tanggal_masuk" required><br>

        <label for="status">Status:</label>
        <select name="status" required>
            <option value="Selesai">Selesai</option>
            <option value="Proses">Proses</option>
            <option value="Belum Diproses">Belum Diproses</option>
        </select><br>

        <!-- Layanan Input -->
        <h3>Layanan</h3>

        <label for="id_layanan_1">ID Layanan 1:</label>
        <input type="text" name="id_layanan[]" required>

        <label for="berat_1">Berat 1:</label>
        <input type="text" name="berat[]" required><br>

        <label for="id_layanan_2">ID Layanan 2:</label>
        <input type="text" name="id_layanan[]" required>

        <label for="berat_2">Berat 2:</label>
        <input type="text" name="berat[]" required><br>

        <!-- Add more layanan input fields if needed -->

        <input type="submit" value="Submit">
    </form>
    </form>
</body>

</html>