<?php
include('layout/header.php');
?>

<body>


    <button class="btn btn-primary">Create New Transaction</h1>

        <form id="transactionForm">
            <label for="id_pelanggan ">Customer ID:</label>
            <input type="text" id="id_pelanggan" name="id_pelanggan" required>
            <br>

            <label for="tanggal_masuk">Pickup Date:</label>
            <input type="date" id="tanggal_masuk" name="tanggal_masuk" required>
            <br>

            <label for="status">Status:</label>
            <select id="status" name="status" required>
                <option value="pending">Pending</option>
                <option value="completed">Completed</option>
                <option value="cancel">Cancel</option>
                <!-- Add other status options as needed -->
            </select>
            <br>

            <label for="layanan">Services:</label>
            <select id="layanan" name="layanan[]" multiple required>
                <!-- Layanan akan diisi secara dinamis oleh skrip JavaScript -->
            </select>
            <br>

            <label for="berat">Weight (kg):</label>
            <input type="number" id="berat" name="berat[]" required>
            <span id="hargaSatuan"></span> USD per kg
            <br>

            <button type="button" id="tambahLayananBtn">Tambah Layanan</button>
            <button type="button" id="hitungTotalBtn">Hitung Total Harga</button>

            <ul id="layananList"></ul>

            <label for="totalHarga">Total Harga:</label>
            <input type="number" id="totalHarga" readonly>
            <br>

            <button type="button" id="createTransactionBtn">Create Transaction</button>

            <script>
                document.addEventListener('DOMContentLoaded', function() {
                    // Fetch layanan data from backend
                    fetch('http://localhost:8080/jenislayanan')
                        .then(response => response.json())
                        .then(data => {
                            console.log('Received layanan data:', data);

                            if (Array.isArray(data)) {
                                // Populate layanan dropdown options
                                const layananSelect = document.getElementById('layanan');
                                data.forEach(layanan => {
                                    const option = document.createElement('option');
                                    option.value = layanan.id_layanan;
                                    option.dataset.harga = layanan.harga;
                                    option.innerText = layanan.nama_layanan;
                                    layananSelect.appendChild(option);
                                });
                            } else {
                                console.error('Invalid layanan data format. Expected an array.');
                            }
                        })
                        .catch(error => {
                            console.error('Error fetching layanan data:', error);
                        });

                    document.getElementById('layanan').addEventListener('change', function() {
                        // Update harga satuan saat layanan berubah
                        const selectedOption = this.options[this.selectedIndex];
                        const hargaSatuan = selectedOption.dataset.harga;
                        document.getElementById('hargaSatuan').innerText = hargaSatuan;
                    });

                    function addLayanan() {
                        const layananSelect = document.getElementById('layanan');
                        const beratInput = document.getElementById('berat');

                        const selectedOption = layananSelect.options[layananSelect.selectedIndex];
                        const id_layanan = selectedOption.value;
                        const layananText = selectedOption.text;
                        const hargaSatuan = parseFloat(selectedOption.dataset.harga);
                        const berat = parseFloat(beratInput.value);

                        // Validasi
                        if (isNaN(berat) || berat <= 0) {
                            alert('Berat harus merupakan angka positif.');
                            return;
                        }

                        // Tambahkan layanan ke daftar
                        const layananList = document.getElementById('layananList');
                        const newItem = document.createElement('li');
                        newItem.innerHTML = `${layananText} (${berat} kg) - Harga: ${hargaSatuan} USD`;
                        newItem.dataset.hargaSatuan = hargaSatuan; // Simpan harga satuan sebagai atribut dataset
                        newItem.dataset.berat = berat; // Simpan berat sebagai atribut dataset
                        layananList.appendChild(newItem);

                        // Reset input
                        beratInput.value = '';
                        layananSelect.selectedIndex = -1;

                        // Hitung total harga
                        hitungTotal();
                    }

                    function hitungTotal() {
                        const layananItems = document.querySelectorAll('#layananList li');
                        const totalHargaInput = document.getElementById('totalHarga');
                        let totalHarga = 0;

                        layananItems.forEach(item => {
                            const hargaSatuan = parseFloat(item.dataset.hargaSatuan);
                            const berat = parseFloat(item.dataset.berat);
                            const subtotal = berat * hargaSatuan;
                            totalHarga += subtotal;
                        });

                        totalHargaInput.value = totalHarga.toFixed(2);
                    }

                    function createTransaction() {
                        const id_pelanggan = document.getElementById('id_pelanggan').value;
                        const tanggal_masuk = document.getElementById('tanggal_masuk').value;
                        const status = document.getElementById('status').value;

                        // Mendapatkan layanan yang dipilih
                        const layananItems = document.querySelectorAll('#layananList li');
                        const layananArray = Array.from(layananItems).map(item => {
                            const regex = /(.+) \((\d+(\.\d+)?) kg\) - Harga: (\d+(\.\d+)?) USD/;
                            const match = regex.exec(item.innerText);
                            return {
                                nama_layanan: match[1],
                                berat: parseFloat(match[2]),
                                harga: parseFloat(match[4])
                            };
                        });

                        // Data transaksi yang akan dikirim
                        const transactionData = {
                            id_pelanggan: id_pelanggan,
                            tanggal_masuk: tanggal_masuk,
                            status: status,
                            layanan: layananArray
                        };

                        // Kirim data transaksi ke backend
                        fetch('http://localhost:8080/transaksi', {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(transactionData)
                            })
                            .then(response => response.json())
                            .then(data => {
                                // Handle respons dari backend (contoh: tampilkan pesan sukses)
                                alert('Transaksi berhasil dibuat dan disimpan ke database!');
                            })
                            .catch(error => {
                                console.error('Error creating transaction:', error);
                            });
                    }


                    // Tambahkan event listener untuk tombol Create Transaction
                    document.getElementById('createTransactionBtn').addEventListener('click', createTransaction);

                    // Dapatkan tombol Tambah Layanan dan tambahkan event listener
                    document.getElementById('tambahLayananBtn').addEventListener('click', addLayanan);

                    // Tambahkan event listener untuk tombol Hitung Total Harga
                    document.getElementById('hitungTotalBtn').addEventListener('click', hitungTotal);
                });
            </script>
</body>

</html>