<?php
echo $this->include('layout/header.php');
?>
<div class="page">
    <?php
    echo $this->include('layout/sidebar.php');
    ?>
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <!-- Page pre-title -->
                        <div class="page-pretitle">
                            Overview
                        </div>
                        <h2 class="page-title">
                            Data pelanggan
                        </h2>
                    </div>
                    <!-- Page title actions -->
                    <div class="col-auto ms-auto d-print-none">
                        <div class="btn-list">
                            <span class="d-none d-sm-inline">
                                <a href="#" class="btn">
                                    New view
                                </a>
                            </span>
                            <a href="/pelanggan/showForm/" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#modal-report">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M12 5l0 14"></path>
                                    <path d="M5 12l14 0"></path>
                                </svg>
                                Create new report
                            </a>
                            <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M12 5l0 14"></path>
                                    <path d="M5 12l14 0"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
                <div class="row row-deck row-cards">
                    <div class="col-md-12">
                        <form class="card">
                            <div class="card-header">
                                <h3 class="card-title">Horizontal form</h3>
                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-vcenter table-mobile-md card-table">

                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>No. HP</th>
                                                    <th>Alamat</th>
                                                    <th>Username</th>
                                                    <th>Password</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <!-- Menampilkan Data Pelanggan -->
                                                <?php foreach ($pelanggan as $pelangganItem) : ?>
                                                    <tr>
                                                        <td><?php echo $pelangganItem['nama']; ?></td>
                                                        <td><?php echo $pelangganItem['no_hp']; ?></td>
                                                        <td><?php echo $pelangganItem['alamat']; ?></td>
                                                        <td><?php echo $pelangganItem['username']; ?></td>
                                                        <td><?php echo $pelangganItem['password']; ?></td>
                                                        <td>
                                                            <!-- Tambahkan tombol edit dan delete sesuai kebutuhan -->
                                                            <a href="<?php echo base_url('pelanggan/edit/' . $pelangganItem['id_pelanggan']); ?>">Edit</a>
                                                            <a href="javascript:void(0);" onclick="deletePelanggan(<?php echo $pelangganItem['id_pelanggan']; ?>);">Delete</a>
                                                            <script>
                                                                function deletePelanggan(id) {
                                                                    var confirmation = confirm("Are you sure you want to delete this customer?");
                                                                    if (confirmation) {
                                                                        // Buat permintaan DELETE dengan JavaScript atau menggunakan library seperti Axios atau Fetch
                                                                        fetch("<?php echo base_url('api/pelanggan/delete/'); ?>" + id, {
                                                                                method: 'DELETE'
                                                                            })
                                                                            .then(response => response.json())
                                                                            .then(data => {
                                                                                // Handle respon dari server
                                                                                console.log(data);
                                                                                // Refresh halaman atau perbarui tampilan data jika diperlukan
                                                                                location.reload(); // Contoh: Refresh halaman setelah penghapusan berhasil
                                                                            })
                                                                            .catch(error => console.error('Error:', error));
                                                                    }
                                                                }
                                                            </script>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo $this->include('layout/footer.php');
        ?>
    </div>
</div>
<?php
echo $this->include('layout/scriptjs.php');
?>