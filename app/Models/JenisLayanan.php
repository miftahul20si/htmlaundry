<?php

namespace App\Models;

use CodeIgniter\Model;

class JenisLayanan extends Model
{
    protected $table         = 'jenislayanan';
    protected $primaryKey    = 'id_layanan';
    protected $allowedFields = ['nama_layanan', 'harga', 'waktu_pengerjaan', 'gambar'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}
