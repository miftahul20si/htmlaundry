<?php

namespace App\Models;

use CodeIgniter\Model;

class TransaksiLayanan extends Model
{
    protected $table = 'transaksilayanan';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_transaksi', 'id_layanan', 'berat', 'total_harga'];

    // Dates
    protected $useTimestamps = false;
}
