<?php

namespace App\Models;

use CodeIgniter\Model;

class Transaksi extends Model
{
    protected $table      = 'layanan';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        // Kolom-kolom lainnya di tabel layanan
    ];

    // Tambahkan relasi ke model Transaksi
    public function transaksi()
    {
        return $this->belongsToMany(Transaksi::class, 'transaksi_layanan', 'id_layanan', 'id_transaksi')
            ->withPivot(['berat', 'total_harga']);
    }
}
