<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTransaksiLayanan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'id_transaksi' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'id_layanan' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'berat' => [
                'type'       => 'INT',
                'constraint' => '4',
            ],
            'total_harga' => [
                'type'       => 'INT',
                'constraint' => '4',
            ],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('transaksi_layanan');
    }

    public function down()
    {
        $this->forge->dropTable('transaksilayanan');
    }
}
