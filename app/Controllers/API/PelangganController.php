<?php

namespace App\Controllers\API;

use App\Models\Pelanggan;
use CodeIgniter\RESTful\ResourceController;

class PelangganController extends ResourceController
{
    protected $modelName = 'App\Models\Pelanggan';
    protected $format = 'json';
    protected $pelangganModel;


    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->pelangganModel = new Pelanggan();
    }

    public function index()
    {
        $data = $this->pelangganModel->findAll();
        return $this->respond($data);
    }


    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id_Pelanggan = null)
    {
        $Pelanggan = $this->pelangganModel->find($id_Pelanggan);

        if ($Pelanggan == null) {
            return $this->failNotFound('Data Pelanggan tidak ditemukan');
        }

        $data = [
            'message' => 'success',
            'Pelanggan_byid' => $Pelanggan
        ];

        return $this->respond($data, 200);
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        $rules = $this->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[Pelanggan.username]',
            'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pelangganModel->insert([
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data Pelanggan berhasil ditambahkan'
        ];

        return $this->respondCreated($response);
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {


        $rules = $this->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[Pelanggan.username,id,' . $id . ']',
            'password' => 'required'
        ]);


        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pelangganModel->update($id, [
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data Pelanggan berhasil diubah'
        ];

        return $this->respond($response, 200);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $this->pelangganModel->delete($id);

        $response = [
            'message' => 'Data Pelanggan berhasil dihapus'
        ];

        return $this->respondDeleted($response);
    }
}
