<?php

namespace App\Controllers\API;

use App\Models\JenisLayanan;
use CodeIgniter\RESTful\ResourceController;

class JenisLayananController extends ResourceController
{
    protected $modelName = 'App\Models\JenisLayanan';
    protected $format = 'json';
    protected $layananModel;


    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->layananModel = new JenisLayanan();
    }

    public function index()
    {
        $data = $this->layananModel->findAll();
        return $this->respond($data);
    }


    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id_layanan = null)
    {
        $data = [
            'message' => 'success',
            'jenislayanan_byid' => $this->layananModel->find($id_layanan)
        ];

        if ($data['jenislayanan_byid'] == null) {
            return $this->failNotFound('Data jenis layanan tidak ditemukan');
        }
        return $this->respond($data, 200);
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        $rules = $this->validate([
            'nama_layanan' => 'required',
            'harga' => 'required',
            'waktu_pengerjaan' => 'required',
            'gambar' => 'uploaded[gambar]|max_size[gambar,2048]|is_image[gambar]|mime_in[gambar,image/jpg,image/jpeg]'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        // Proses upload
        $gambar = $this->request->getFile('gambar');
        $namaGambar = $gambar->getRandomName();
        $gambar->move('gambar', $namaGambar);

        $this->layananModel->insert([
            'nama_layanan' => esc($this->request->getVar('nama_layanan')),
            'harga' => esc($this->request->getVar('harga')),
            'waktu_pengerjaan' => esc($this->request->getVar('waktu_pengerjaan')),
            'gambar' => $namaGambar
        ]);

        $response = [
            'message' => 'Data jenis layanan berhasil ditambahkan'
        ];

        return $this->respondCreated($response);
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        // Validasi gambar hanya jika ada gambar yang diunggah
        $rules = $this->validate([
            'gambar' => 'max_size[gambar,2048]|is_image[gambar]|mime_in[gambar,image/jpg,image/jpeg]'
        ]);

        if (!$rules && $this->request->getFile('gambar')) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        // Proses upload
        $gambar = $this->request->getFile('gambar');
        $gambarLama = $this->request->getPost('gambarLama') ?? '';

        if ($gambar) {
            $namaGambar = $gambar->getRandomName();
            $gambar->move('gambar', $namaGambar);

            // Menghapus gambar lama jika ada perubahan gambar
            if ($gambarLama && $gambarLama != $namaGambar) {
                unlink('gambar/' . $gambarLama);
            }
        } else {
            $namaGambar = $gambarLama;
        }

        $this->layananModel->update($id, [
            'nama_layanan' => esc($this->request->getVar('nama_layanan')),
            'harga' => esc($this->request->getVar('harga')),
            'waktu_pengerjaan' => esc($this->request->getVar('waktu_pengerjaan')),
            'gambar' => $namaGambar
        ]);

        $response = [
            'data' => 'data succes be update'
        ];

        return $this->respond($response, 200);
    }



    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $gambarDb = $this->layananModel->find($id);

        if ($gambarDb['gambar'] != '') {
            unlink('gambar/' . $gambarDb['gambar']);
        }

        $this->layananModel->delete($id);

        $response = [
            'message' => 'Data jenis layanan berhasil dihapus'
        ];

        return $this->respondDeleted($response);
    }
}
