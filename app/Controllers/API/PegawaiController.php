<?php

namespace App\Controllers\API;

use App\Models\Pegawai;
use CodeIgniter\RESTful\ResourceController;

class PegawaiController extends ResourceController
{
    protected $modelName = 'App\Models\Pegawai';
    protected $format = 'json';
    protected $pegawaiModel;
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */

    public function __construct()
    {
        $this->pegawaiModel = new Pegawai();
    }

    public function index()
    {
        $data = $this->pegawaiModel->findAll();
        return $this->respond($data);
    }

    public function show($id = null)
    {
        $pegawai = $this->pegawaiModel->find($id);

        if (!$pegawai) {
            return $this->failNotFound('Pegawai not found');
        }

        return $this->respond($pegawai);
    }



    public function create()
    {
        $rules = $this->validate([
            'nama' => 'required',
            // 'no_hp' => 'required',
            // 'alamat' => 'required',
            // 'username' => 'required|is_unique[pegawai.username]',
            // 'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pegawaiModel->insert([
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data pegawai berhasil ditambahkan'
        ];

        return $this->respondCreated($response);
    }

    public function update($id = null)
    {
        $rules = $this->validate([
            'nama' => 'required',
            // 'no_hp' => 'required',
            // 'alamat' => 'required',
            // 'username' => 'required|is_unique[pegawai.username,id_pegawai,' . $id . ']',
            // 'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pegawaiModel->update($id, [
            'nama' => esc($this->request->getVar('nama')),
            // 'no_hp' => esc($this->request->getVar('no_hp')),
            // 'alamat' => esc($this->request->getVar('alamat')),
            // 'username' => esc($this->request->getVar('username')),
            // 'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data pegawai berhasil diubah'
        ];

        return $this->respond($response, 200);
    }

    public function delete($id = null)
    {
        $this->pegawaiModel->delete($id);

        $response = [
            'message' => 'Data pegawai berhasil dihapus'
        ];

        return $this->respondDeleted($response);
    }
}
