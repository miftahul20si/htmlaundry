<?php

namespace App\Controllers\API;

use App\Models\Transaksi;
use App\Models\TransaksiLayanan;
use CodeIgniter\RESTful\ResourceController;

class TransaksiController extends ResourceController
{
    protected $modelName = 'App\Models\Transaksi';
    protected $format = 'json';

    public function index()
    {
        // Ambil data pegawai dari model
        $transaksiModel = new \App\Models\Transaksi(); // Sesuaikan dengan nama model yang Anda gunakan
        $data['transaksi'] = $transaksiModel->findAll();

        return view('transaksi/index', $data);
    }

    public function showForm()
    {
        return view('transaksi/formcoba');
    }


    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        // Logika untuk membuat transaksi dan menyimpan data pivot layanan_transaksi
        $transaksiModel = new Transaksi();
        $layananTransaksiModel = new TransaksiLayanan();

        // Contoh data transaksi
        $transaksiData = [
            'id_pegawai' => 1,
            'id_pelanggan' => 2,
            'total_harga' => 100.00,
            'tanggal_masuk' => '2024-01-01',
            'tanggal_selesai' => '2024-01-10',
            'status' => 'Selesai',
            'keluhan' => 'Layanan bagus',
            'gambar' => 'gambar.jpg',
        ];

        // Simpan transaksi
        $idTransaksi = $transaksiModel->insert($transaksiData);

        // Contoh data layanan_transaksi (pivot)
        $layananTransaksiData = [
            'id_transaksi' => $idTransaksi,
            'id_layanan' => 3,
            'berat' => 5,
            'total_harga' => 50,
        ];

        // Simpan data pivot layanan_transaksi
        $layananTransaksiModel->insert($layananTransaksiData);

        // Response atau redirect sesuai kebutuhan
        return 'Transaksi berhasil disimpan';
    }

    // public function create()
    // {
    //     $rules = $this->validate([
    //         'id_pelanggan' => 'required',
    //         'tanggal_masuk' => 'required',
    //         'status' => 'required',
    //         'layanan' => 'required',
    //     ]);

    //     if (!$rules) {
    //         $response = [
    //             'message' => $this->validator->getErrors()
    //         ];

    //         return $this->failValidationErrors($response);
    //     }

    //     // Data Transaksi
    //     $transaksiData = [
    //         'id_pelanggan' => esc($this->request->getVar('id_pelanggan')),
    //         'tanggal_masuk' => esc($this->request->getVar('tanggal_masuk')),
    //         'status' => esc($this->request->getVar('status')),
    //         'layanan' => esc($this->request->getVar('layanan')),
    //     ];

    //     // Insert Transaksi
    //     $transaksiId = $this->model->insert($transaksiData);

    //     if (!$transaksiId) {
    //         $response = [
    //             'message' => 'Failed to create transaction'
    //         ];

    //         return $this->fail($response);
    //     }

    //     // Data Layanan (Jenis Layanan)
    //     $layananData = $this->request->getVar('layanan');
    //     $beratData = $this->request->getVar('berat');

    //     // Insert ke Tabel Pivot
    //     $transaksiLayananModel = new \App\Models\TransaksiLayanan();
    //     foreach ($layananData as $index => $layananId) {
    //         $pivotData = [
    //             'id_transaksi' => $transaksiId,
    //             'id_layanan' => $layananId,
    //             'berat' => $beratData[$index],
    //         ];

    //         // Insert ke Tabel Pivot (transaksi_layanan)
    //         $transaksiLayananModel->insert($pivotData);
    //     }

    //     $response = [
    //         'message' => 'Transaction created successfully'
    //     ];

    //     return $this->respondCreated($response);
    // }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $rules = $this->validate([
            // 'id_pelanggan' => 'required',
            'tanggal_antar' => 'required',
            // 'tanggal_masuk' => 'required',
            'status' => 'required',
        ]);

        if (!$rules) {
            $response = [
                'message' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->model->update($id, [
            // 'id_pelanggan' => esc($this->request->getVar('id_pelanggan')),
            'tanggal_antar' => esc($this->request->getVar('tanggal_antar')),
            // 'tanggal_masuk' => esc($this->request->getVar('tanggal_masuk')),
            'status' => esc($this->request->getVar('status')),
        ]);
        $response = [
            'message' => 'Data antar jemput berhasil diubah'
        ];
        return $this->respond($response, 200);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $this->model->delete($id);

        $response = [
            'message' => 'Data antar jemput berhasil dihapus'
        ];
        return $this->respondDeleted($response);
    }
}
