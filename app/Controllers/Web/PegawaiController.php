<?php

namespace App\Controllers\Web;

use App\Models\Pegawai;
use CodeIgniter\RESTful\ResourceController;

class PegawaiController extends ResourceController
{
    protected $modelName = 'App\Models\Pegawai';
    protected $format = 'json';
    protected $pegawaiModel;
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */

    public function __construct()
    {
        $this->pegawaiModel = new Pegawai();
    }

    public function index()
    {
        $data['pegawai'] = $this->pegawaiModel->findAll();
        return view('pegawai/index', $data);
    }

    public function showForm()
    {
        $data['pegawai'] = $this->pegawaiModel->findAll();

        log_message('debug', 'Pegawai data: ' . print_r($data['pegawai'], true));

        return view('pegawai/formpegawai', $data);
    }


    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id_pegawai = null)
    {
        $pegawai = $this->pegawaiModel->find($id_pegawai);

        if ($pegawai == null) {
            return $this->failNotFound('Data pegawai tidak ditemukan');
        }

        return $this->respond(['message' => 'success', 'pegawai_byid' => $pegawai], 200);
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        return view('pegawai/formpegawai');
    }

    public function store()
    {
        // Validasi input jika diperlukan
        $rules = [
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[pegawai.username]',
            'password' => 'required'
        ];

        if (!$this->validate($rules)) {
            $data = [
                'validation' => $this->validator
            ];

            return view('pegawai/formpegawai', $data);
        }

        // Simpan data ke dalam database
        $inserted = $this->pegawaiModel->insert([
            'nama' => $this->request->getVar('nama'),
            'no_hp' => $this->request->getVar('no_handphone'), // perhatikan penamaan field
            'alamat' => $this->request->getVar('alamat'),
            'username' => $this->request->getVar('username'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        // Tambahkan log pesan
        if ($inserted) {
            log_message('debug', 'Data pegawai berhasil disimpan');
        } else {
            log_message('debug', 'Gagal menyimpan data pegawai');
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $rules = $this->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[pegawai.username,id_pegawai,' . $id . ']',
            'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pegawaiModel->update($id, [
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data pegawai berhasil diubah'
        ];

        return $this->respond($response, 200);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $pegawai = $this->pegawaiModel->find($id);

        if ($pegawai == null) {
            return $this->failNotFound('Data pegawai tidak ditemukan');
        }

        $this->pegawaiModel->delete($id);

        return $this->respondDeleted(['message' => 'Data pegawai berhasil dihapus']);
    }
}
