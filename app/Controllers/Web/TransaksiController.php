<?php

namespace App\Controllers\Web;

use App\Models\Transaksi;
use App\Models\TransaksiLayanan;
use CodeIgniter\RESTful\ResourceController;

class TransaksiController extends ResourceController
{
    protected $modelName = 'App\Models\Transaksi';
    protected $format = 'json';

    public function index()
    {
        // Ambil data transaksi dari model
        $transaksiModel = new \App\Models\Transaksi();
        $data['transaksi'] = $transaksiModel->findAll();

        return view('transaksi/index', $data);
    }

    public function showForm()
    {
        return view('transaksi/formcoba');
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        // Logika untuk membuat transaksi dan menyimpan data pivot layanan_transaksi
        $transaksiModel = new Transaksi();
        $layananTransaksiModel = new TransaksiLayanan();

        // Contoh data transaksi
        $transaksiData = [
            'id_pelanggan' => 1,
            'tanggal_masuk' => '2024-01-01',
            'status' => 'Selesai',
        ];

        // Simpan transaksi
        $idTransaksi = $transaksiModel->insert($transaksiData);

        // Contoh data layanan_transaksi (pivot)
        $layananTransaksiData = [
            'id_transaksi' => $idTransaksi,
            'id_layanan' => 3,
            'berat' => 5,
            'total_harga' => 50,
        ];

        // Simpan data pivot layanan_transaksi
        $layananTransaksiModel->insert($layananTransaksiData);

        // Response atau redirect sesuai kebutuhan
        return 'Transaksi berhasil disimpan';
    }

    /**
     * Update the specified resource
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $rules = $this->validate([
            'tanggal_antar' => 'required',
            'status' => 'required',
        ]);

        if (!$rules) {
            $response = [
                'message' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->model->update($id, [
            'tanggal_antar' => esc($this->request->getVar('tanggal_antar')),
            'status' => esc($this->request->getVar('status')),
        ]);

        $response = [
            'message' => 'Data antar jemput berhasil diubah'
        ];
        return $this->respond($response, 200);
    }

    /**
     * Delete the specified resource
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $this->model->delete($id);

        $response = [
            'message' => 'Data antar jemput berhasil dihapus'
        ];
        return $this->respondDeleted($response);
    }
}
