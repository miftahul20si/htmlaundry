<?php

namespace App\Controllers\Web;

use App\Models\Pelanggan;
use CodeIgniter\RESTful\ResourceController;

class PelangganController extends ResourceController
{
    protected $modelName = 'App\Models\Pelanggan';
    protected $format = 'json';
    protected $pelangganModel;

    public function __construct()
    {
        $this->pelangganModel = new Pelanggan();
    }

    public function index()
    {
        $data['pelanggan'] = $this->pelangganModel->findAll();
        return view('pelanggan/index', $data);
    }

    public function showForm()
    {
        $data['pelanggan'] = $this->pelangganModel->findAll();
        return view('pelanggan/formpelanggan', $data);
    }

    public function show($id_pelanggan = null)
    {
        $pelanggan = $this->pelangganModel->find($id_pelanggan);

        if ($pelanggan == null) {
            return $this->failNotFound('Data pelanggan tidak ditemukan');
        }

        $data = [
            'message' => 'success',
            'pelanggan_byid' => $pelanggan
        ];

        return $this->respond($data, 200);
    }

    public function create()
    {
        $rules = $this->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[pelanggan.username]',
            'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pelangganModel->insert([
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data pelanggan berhasil ditambahkan'
        ];

        return $this->respondCreated($response);
    }

    public function update($id = null)
    {
        $rules = $this->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'username' => 'required|is_unique[pelanggan.username,id_pelanggan,' . $id . ']',
            'password' => 'required'
        ]);

        if (!$rules) {
            $response = [
                'message' => 'Validation failed',
                'errors' => $this->validator->getErrors()
            ];

            return $this->failValidationErrors($response);
        }

        $this->pelangganModel->update($id, [
            'nama' => esc($this->request->getVar('nama')),
            'no_hp' => esc($this->request->getVar('no_hp')),
            'alamat' => esc($this->request->getVar('alamat')),
            'username' => esc($this->request->getVar('username')),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);

        $response = [
            'message' => 'Data pelanggan berhasil diubah'
        ];

        return $this->respond($response, 200);
    }

    public function delete($id = null)
    {
        $this->pelangganModel->delete($id);

        $response = [
            'message' => 'Data pelanggan berhasil dihapus'
        ];

        return $this->respondDeleted($response);
    }
}
